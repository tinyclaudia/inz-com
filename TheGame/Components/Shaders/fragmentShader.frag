#version 330 core

uniform sampler2D ourTexture;

uniform mat2 projectionTexture;
uniform vec2 animationOffset;

float cutoff = 0.1;

in vec2 TexCoord;

void main(void)
{
 gl_FragColor = texture(ourTexture,projectionTexture * (TexCoord+animationOffset));
 
 if (gl_FragColor.a < cutoff)
        // alpha value less than user-specified threshold?
    {
        discard; // yes: discard this fragment
    }
}