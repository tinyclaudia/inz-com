#version 330 core

layout (location = 0) in vec4 position;
layout (location = 1) in vec2 aTexCoord;

out vec2 TexCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main(void)
{
 gl_Position = projection * view * model * position;
 TexCoord = aTexCoord;
}