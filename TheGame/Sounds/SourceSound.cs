﻿using System;
using OpenTK.Audio.OpenAL;
using System.IO;
using OpenTK.Audio;

namespace Sounds
{
    public class SourceSound: IDisposable
    {
        int source;
        int[] gBuffers;
        ALSourceState state;
        private static AudioContext audioContex = null;

        private AudioContext GetAudioContext()
        {
            if(audioContex == null)
            {
                audioContex = new AudioContext();
            }

            return audioContex;
        }

        public SourceSound(string fileNameSound)
        {

            audioContex = GetAudioContext();
           
            // Generate Buffers 
            AL.GetError(); // clear error code 
            gBuffers = AL.GenBuffers(1);
            source = AL.GenSource();


            var error = AL.GetError();
            if (error != ALError.NoError)
            {
                Console.WriteLine($"alGenBuffers :{error}");
                return;
            }

            int channels, bits_per_sample, sample_rate;
            byte[] sound_data = LoadWave(File.Open(fileNameSound, FileMode.Open), out channels, out bits_per_sample, out sample_rate);
            AL.BufferData(gBuffers[0], GetSoundFormat(channels, bits_per_sample), sound_data, sound_data.Length, sample_rate);

            AL.Source(source, ALSourcei.Buffer, gBuffers[0]);
            AL.SourcePlay(source);

            AL.GetSource(source, ALGetSourcei.SourceState, out int istate);



                //Thread.Sleep(10000);            
        }

        public ALSourceState GetState()
        {
            AL.GetSource(source, ALGetSourcei.SourceState, out int istate);

            return (ALSourceState)istate;
        }
        // https://github.com/mono/opentk/blob/master/Source/Examples/OpenAL/1.1/Playback.cs
        public static byte[] LoadWave(Stream stream, out int channels, out int bits, out int rate)
        {
            if (stream == null)
                throw new ArgumentNullException("stream");

            using (BinaryReader reader = new BinaryReader(stream))
            {
                // RIFF header
                string signature = new string(reader.ReadChars(4));
                if (signature != "RIFF")
                    throw new NotSupportedException("Specified stream is not a wave file.");

                int riff_chunck_size = reader.ReadInt32();

                string format = new string(reader.ReadChars(4));
                if (format != "WAVE")
                    throw new NotSupportedException("Specified stream is not a wave file.");

                // WAVE header
                string format_signature = new string(reader.ReadChars(4));
                if (format_signature != "fmt ")
                    throw new NotSupportedException("Specified wave file is not supported.");

                int format_chunk_size = reader.ReadInt32();
                int audio_format = reader.ReadInt16();
                int num_channels = reader.ReadInt16();
                int sample_rate = reader.ReadInt32();
                int byte_rate = reader.ReadInt32();
                int block_align = reader.ReadInt16();
                int bits_per_sample = reader.ReadInt16();

                string data_signature = new string(reader.ReadChars(4));
                if (data_signature != "data")
                    throw new NotSupportedException("Specified wave file is not supported.");

                int data_chunk_size = reader.ReadInt32();

                channels = num_channels;
                bits = bits_per_sample;
                rate = sample_rate;

                return reader.ReadBytes((int)reader.BaseStream.Length);
            }
        }

        public static ALFormat GetSoundFormat(int channels, int bits)
        {
            switch (channels)
            {
                case 1: return bits == 8 ? ALFormat.Mono8 : ALFormat.Mono16;
                case 2: return bits == 8 ? ALFormat.Stereo8 : ALFormat.Stereo16;
                default: throw new NotSupportedException("The specified sound format is not supported.");
            }
        }

        bool wasDisposed = false;
        private void dispose()
        {
            if(!wasDisposed)
            {
                AL.SourceStop(source);
                AL.DeleteSource(source);
                AL.DeleteBuffer(gBuffers[0]);
                audioContex.Dispose();

                wasDisposed = true;
            }
        }

        public void Dispose()
        {
            dispose();
        }
        ~SourceSound()
        {
            dispose();
        }
    }
}
