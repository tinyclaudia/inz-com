﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace logger
{
    public static class Logger
    {
        public static void LogInfo(string message)
        {
            var sb = new StringBuilder();
            sb.Append("[INFO]  ");
            sb.Append(message);
        }

        public static void LogError(string message)
        {
            var sb = new StringBuilder();
            sb.Append("[ERROR] ");
            sb.Append(message);
        }
    }
}
