﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Graphics.Meshes;
using Graphics.Systems;

namespace Graphics.Objects
{
    class GameObject
    {
        protected IMesh mesh;

        public int Id { get;}

        protected delegate void ObjectAction();

        void MoveObject(int x, int y)
        {
            mesh.TranslateLocation(x,y);
        }

        public GameObject(IMesh mesh, int id)
        {
            Id = id;
        }

        public virtual void CallMethod(string method)
        {
        }
    }
}
