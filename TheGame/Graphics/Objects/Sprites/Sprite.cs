﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;
using OpenTK;

namespace Graphics.Objects.Sprites
{
    class Sprite : ISprite
    {
        Texture RenderedTexture;

        Vector2 offsetAnimation;
        int numFrames;
        float widthSprite;
        public int frame;

        public float Width { get { return RenderedTexture.Width; } }
        public float Height { get { return RenderedTexture.Height; } }

        public void Bind(int programID, uint unit)
        {

            int animationOffsetLoc = GL.GetUniformLocation(programID, "animationOffset");
            GL.Uniform2(animationOffsetLoc, ref offsetAnimation);

            RenderedTexture.Bind(unit);
        }
        public void ChangeFrame(int i)
        {
            frame = i;
            offsetAnimation.X = widthSprite * i;
        }
        public void NextFrame()
        {
            ChangeFrame((frame + 1)%numFrames);
        }

        public int GetFrameNumber()
        {
            return frame;
        }

        public double FrameTime { get; private set; }

        public bool IsAnimated { get; private set; }

        public double FrameCounter { get; set; }

        public void SetAnimation(bool isStarting, int? frameTime = null)
        {
            IsAnimated = isStarting;
        }

        public Sprite(string textureFileName, int _numFrames, double frameTime = 0.2)
        {
            if(textureFileName == null)
            {
                throw new Exception();
            }

            RenderedTexture = new Texture(textureFileName);

            offsetAnimation = new Vector2(0,0);
            FrameTime = frameTime;
            numFrames = _numFrames;
            widthSprite = RenderedTexture.width / numFrames;

            frame = 0;
        }
    }
}
