﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics.Objects.Sprites
{
    class ConstSprite : ISprite
    {
        Texture RenderedTexture;

        public float Width { get { return RenderedTexture.Width; } }
        public float Height { get { return RenderedTexture.Height; } }

        public void Bind(int programID, uint unit = 0)
        {
            RenderedTexture.Bind(unit);
        }

        public void ChangeFrame(int i)
        {
            throw new Exception();
        }

        public bool IsAnimated { get; }
        public double FrameTime { get; }

        public double FrameCounter
        {
            get => 0;
            set { }
        }

        public void NextFrame()
        {
        }

        public int GetFrameNumber()
        {
            return 0;
        }

        public void SetAnimation(bool isAnimated, int? frameTime)
        {
        }

        public ConstSprite(string texturesFileNames)
        {
            RenderedTexture = new Texture(texturesFileNames);
        }
    }
}
