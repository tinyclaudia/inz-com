﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics.Objects.Sprites
{
    interface ISprite
    {
        void Bind(int programID, uint unit = 0);
        float Width { get; }
        float Height { get; }
        /// <summary>
        /// change frame of Sprite
        /// </summary>
        /// <param name="i">number of frame (must be [0 ;numFrames) )</param>
        void ChangeFrame(int i);
        bool IsAnimated { get; }

        double FrameTime { get; }

        void SetAnimation(bool isAnimated, int? frameTime);

        double FrameCounter { get; set; }
        void NextFrame();
        int GetFrameNumber();
        
    }
}
