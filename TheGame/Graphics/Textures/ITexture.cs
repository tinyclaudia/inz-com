﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics.Textures
{
    interface ITexture : IDisposable
    {
        void Bind(uint unit);
        float Width { get; }
        float Height { get; }
    }
}
