﻿using System;
using Graphics.Meshes;
using Graphics.Objects.Sprites;
using Graphics.Systems;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Graphics
{

    class Mesh : IMesh,IDisposable
    {
        protected int vertexArray;
        protected int verticeCount;
        protected Matrix4 model;
        private Vector2 animationOffset = new Vector2(0, 0);

        static Matrix4 projection  = Matrix4.CreateOrthographicOffCenter(0.0f, ConstVal.WidthView, ConstVal.HeightView, 0.0f, -1.0f, 1.0f);

        //program which will draw mesh
        protected Shader shader;

        public int IndexSprite { get; set; }
        /// <summary>
        /// sluzy do renderowania tekstury mesh'a
        /// </summary>
        public ISprite renderedSprite;
        /// <summary>
        /// Zbior sprite'ów (gdy potrzebna jest animacja)
        /// </summary>
        protected ISprite [] sprites;
        //switch from texcord to (0,1)x(0,1) coords
        public Matrix2 projectionTexture = Matrix2.Identity;

        private int [] vertexArrayBuffers = new int[2];
        /// <summary>
        /// stwórz program który będzie odpowiedzialny za obliczanie koloru piksela dla mesha
        /// </summary>
        /// <returns></returns>
        protected virtual Shader CreateShader()
        {
            return new Shader();
        }
        /// <summary>
        /// zmien Sprite'a
        /// </summary>
        /// <param name="i">numer Sprite'a</param>
        public void ChangeSprite(int i)
        {
            IndexSprite = i;
            renderedSprite = sprites[IndexSprite];
            renderedSprite.ChangeFrame(0);
            CreateProjectionTexture();
        }

        /// <summary>
        /// 
        /// </summary>
        public int SpeedChangingSprite { get; set; }

        private bool isAnimated;
        public bool IsAnimated
        {
            get=> isAnimated;
            set
            {
                isAnimated = value;
                if (!value)
                {
                    renderedSprite.ChangeFrame(0);
                }
            }
        }

        /// <summary>
        /// Zmien polozenie Mesha o wektor v = (x,y)
        /// </summary>
        /// <param name="x">x wspolrzedna wektora translacji</param>
        /// <param name="y">y wsporzedna wektora translacji</param>
        public void TranslateLocation(float x, float y)
        {
            //add to cells in matrix
            model.M41 += x;
            model.M42 += y;
        }
        /// <summary>
        /// wyślij za pomocą funkcji OpenGL'a atrybuty vertices i texCoords aby wyrenderowac poprawnie mesha
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="texCoords"></param>
        private void SendAttributesToGraphics(Vector4[] vertices, Vector2[] texCoords)
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexArrayBuffers[0]);
            GL.BufferData(BufferTarget.ArrayBuffer, verticeCount * Vector4.SizeInBytes, vertices, BufferUsageHint.StaticDraw);

            GL.EnableVertexAttribArray(0);//data in 0 atrib
            GL.VertexAttribPointer(0, 4, VertexAttribPointerType.Float, false, 0, 0);

            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexArrayBuffers[1]);
            GL.BufferData(BufferTarget.ArrayBuffer, verticeCount * Vector2.SizeInBytes, texCoords, BufferUsageHint.StaticDraw);

            GL.EnableVertexAttribArray(1);//data in 1 atrib
            GL.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, false, 0, 0);
        }
        /// <summary>
        /// stwórz macierz projekcji tekstury (tak aby podawać w texCoords wsporzedne w pikselach)
        /// </summary>
        private void CreateProjectionTexture()
        {
            //cells in matrix
            projectionTexture.M11 = 1.0f / renderedSprite.Width;
            projectionTexture.M22 = 1.0f / renderedSprite.Height;
        }

        public Mesh(Vector4 [] vertices,Vector2 [] texCoords, string textureFileName)
        {
            shader = CreateShader();
            renderedSprite = new ConstSprite(textureFileName);
            CreateProjectionTexture();

            InitializeGLComponentsForMeshRendering(vertices, texCoords);

            RenderingSystem.AddMesh(this);
            ChangingSpriteSystem.AddToChangingSpriteSystem(this);
        }

        public Mesh(Vector4[] vertices, Vector2[] texCoords, Sprite _sprite)
        {
            shader = CreateShader();
            renderedSprite = _sprite;
            CreateProjectionTexture();

            InitializeGLComponentsForMeshRendering(vertices, texCoords);

            RenderingSystem.AddMesh(this);
            ChangingSpriteSystem.AddToChangingSpriteSystem(this);
        }

        public Mesh(Vector4[] vertices, Vector2[] texCoords, ISprite [] _sprite)
        {

            shader = CreateShader();

            sprites = _sprite;

            renderedSprite = sprites[0];
            CreateProjectionTexture();

            InitializeGLComponentsForMeshRendering(vertices, texCoords);
           
            RenderingSystem.AddMesh(this);
            ChangingSpriteSystem.AddToChangingSpriteSystem(this);
        }
        private void InitializeGLComponentsForMeshRendering(Vector4[] vertices, Vector2[] texCoords)
        {
            verticeCount = vertices.Length;

            model = Matrix4.Identity;

            vertexArray = GL.GenVertexArray();
            GL.BindVertexArray(vertexArray);

            GL.GenBuffers(2, vertexArrayBuffers);

            SendAttributesToGraphics(vertices, texCoords);

            GL.BindVertexArray(0);

            GL.UseProgram(shader.ProgramID);

            

            int projectionTextureLoc = GL.GetUniformLocation(shader.ProgramID, "projectionTexture");
            GL.UniformMatrix2(projectionTextureLoc, false, ref projectionTexture);
        }

        public void Render()
        {

            GL.UseProgram(shader.ProgramID);

            int modelLoc = GL.GetUniformLocation(shader.ProgramID, "model");
            GL.UniformMatrix4(modelLoc, false, ref model);

            Camera.MainCamera.Bind(shader.ProgramID);

            renderedSprite.Bind(shader.ProgramID,0);

            GL.BindVertexArray(vertexArray);
            GL.DrawArrays(PrimitiveType.Triangles, 0, verticeCount);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    GL.DeleteVertexArray(vertexArray);
                    shader.Dispose();
                    // TODO: dispose managed state (managed objects).
                }
                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }

        public void NextSprite()
        {
            IndexSprite = (IndexSprite + 1) % sprites.Length;
            ChangeSprite(IndexSprite);
        }

        public ISprite GetRenderedSprite()
        {
            return renderedSprite;
        }


        #endregion
    }
}
