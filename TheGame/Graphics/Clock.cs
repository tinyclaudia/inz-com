﻿using System;

namespace Graphics
{
    class Clock
    {
        double time;
        double period;
        public Clock(double _period,double _time = 0)
        {
            time = _time;
            period = _period;
        }

        public void AddTime(double deltaTime)
        {
            time = time + deltaTime;

            if(time >= period)
            {
                time -= period;
                OnCountdownCompleted(new EventArgs());
            }
        }
        public event EventHandler CountdownCompleted;

        protected virtual void OnCountdownCompleted(EventArgs e)
        {
            CountdownCompleted?.Invoke(this, e);
        }
    }
}
