﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Graphics.Meshes;

namespace Graphics.Systems
{
    static class RenderingSystem
    {
        private static List<IMesh> meshesToRender = new List<IMesh>();

        public static void Update()
        {
            Sort();
            foreach (IMesh mesh in meshesToRender)
            {
               mesh.Render(); 
            }
        }

        private static void Sort()
        {
            //meshesToRender.Sort(y);
        }

        public static void AddMesh(IMesh m)
        {
            meshesToRender.Add(m);
        }
    }
}
