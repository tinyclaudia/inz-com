﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Graphics.Meshes;
using Graphics.Objects.Sprites;

namespace Graphics.Systems
{
    static class ChangingSpriteSystem
    {
        private static List<IMesh> listMesh = new List<IMesh>();

        public static void AddToChangingSpriteSystem(IMesh mesh)
        {
            listMesh.Add(mesh);
        }

        public static void Update(double deltaTime)
        {
            foreach (IMesh mesh in listMesh)
            {
                if (mesh.IsAnimated)
                {
                    UpdateFrameCounter(mesh, deltaTime);
                }
            }
        }

        private static void UpdateFrameCounter(IMesh mesh, double deltaTime)
        {
            ISprite sprite = mesh.GetRenderedSprite();

            sprite.FrameCounter += deltaTime;

            if (sprite.FrameCounter > sprite.FrameTime)
            {
                sprite.FrameCounter -= sprite.FrameTime;
                sprite.NextFrame();
            }
        }
    }
}
