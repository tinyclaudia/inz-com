﻿using System;
using System.Drawing;
using OpenTK.Graphics.OpenGL;
using System.Drawing.Imaging;

namespace Graphics
{

    public class Texture: IDisposable,Textures.ITexture
    {
        private int textureGL;
        public float width, height;

        public Texture(string fileName)
        {
            Bitmap bitmap = new Bitmap(fileName);

            width = bitmap.Width;
            height = bitmap.Height;

            var data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            //generate space for Texture
            textureGL = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, textureGL);
           
            //wrap texture and repeat
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)All.Repeat);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)All.Repeat);

            //linear interpollation
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)All.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)All.Nearest);

            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, (int)width, (int)height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);
            //GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);

            bitmap.UnlockBits(data);
            bitmap.Dispose();
        }

        public void Bind(uint unit)
        {
            var textureUnit = (TextureUnit)((int)TextureUnit.Texture0 + unit);

            GL.ActiveTexture(textureUnit);
            GL.BindTexture(TextureTarget.Texture2D, textureGL);
        }

        public float Width { get { return width; } }

        public float Height { get { return height; } }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    GL.DeleteTexture(textureGL);
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~Texture() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
