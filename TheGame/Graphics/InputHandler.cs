﻿using OpenTK.Input;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Graphics
{
    struct KeyState
    {
        public Key GameKey { get; set; }
        public bool IsOnDown { get; set; }

        public KeyState(Key gameKey, bool isOnDown)
        {
            GameKey = gameKey;
            IsOnDown = isOnDown;
        }
    }
    enum GameKeys
    {
        PLAYER_MOVE_RIGHT,
        PLAYER_MOVE_LEFT,
        PLAYER_MOVE_UP,
        PLAYER_MOVE_DOWN,
        ZOOM_IN,
        ZOOM_OUT
    };
    class InputCommand
    {
        public InputCommand(Action _command,GameKeys _gameKey)
        {
            command = _command;
            GameKey = _gameKey;
            HasToExecute = false;
        }
        public void Execute() => command();
        public bool HasToExecute { get; set; }
        public GameKeys GameKey { get; set; }
        Action command;
    }
    class InputHandler
    {
        public static InputHandler keyboardHandler = new InputHandler();

        Dictionary<KeyState, InputCommand> mapKeys = new Dictionary<KeyState,InputCommand>()
        {

        };

        /// <summary>
        /// pomocnicza lista
        /// </summary>
        List<Tuple<Key, bool,InputCommand>> listGameKeys = new List<Tuple<Key, bool,InputCommand>>()
        {
            new Tuple<Key,bool,InputCommand>(Key.W, true, new InputCommand(Window.GetWindow().PlayerMoveUp, GameKeys.PLAYER_MOVE_UP)),
            new Tuple<Key,bool,InputCommand>(Key.S, true, new InputCommand(Window.GetWindow().PlayerMoveDown, GameKeys.PLAYER_MOVE_DOWN)),
            new Tuple<Key,bool,InputCommand>(Key.A, true, new InputCommand(Window.GetWindow().PlayerMoveLeft, GameKeys.PLAYER_MOVE_LEFT)),
            new Tuple<Key,bool,InputCommand>(Key.D, true, new InputCommand(Window.GetWindow().PlayerMoveRight, GameKeys.PLAYER_MOVE_RIGHT)),
            new Tuple<Key,bool,InputCommand>(Key.Y, true, new InputCommand(Window.GetWindow().CameraZoomIn, GameKeys.ZOOM_IN)),
            new Tuple<Key,bool,InputCommand>(Key.T, true, new InputCommand(Window.GetWindow().CameraZoomOut, GameKeys.ZOOM_OUT)),
            new Tuple<Key,bool,InputCommand>(Key.W, false, new InputCommand(Window.GetWindow().PlayerMoveEnd, GameKeys.PLAYER_MOVE_UP)),
            new Tuple<Key,bool,InputCommand>(Key.S, false, new InputCommand(Window.GetWindow().PlayerMoveEnd, GameKeys.PLAYER_MOVE_DOWN)),
            new Tuple<Key,bool,InputCommand>(Key.A, false, new InputCommand(Window.GetWindow().PlayerMoveEnd, GameKeys.PLAYER_MOVE_LEFT)),
            new Tuple<Key,bool,InputCommand>(Key.D, false, new InputCommand(Window.GetWindow().PlayerMoveEnd, GameKeys.PLAYER_MOVE_RIGHT)),
        };

        private void AddDefaultValuesToMaps()
        {
            for (int i = 0; i < listGameKeys.Count; i++)
            {
                mapKeys.Add(new KeyState(listGameKeys[i].Item1,listGameKeys[i].Item2), listGameKeys[i].Item3);
            }
        }

        private InputHandler()
        {
            AddDefaultValuesToMaps();
        }

        private List<GameKeys> keysUp = new List<GameKeys>();

        internal void Update()
        {
            if(Window.GetWindow().Focused)
            {
                var keyboard = Keyboard.GetState();

                if (keyboard.IsAnyKeyDown)
                {
                    foreach (var pair in mapKeys.Where(p => p.Key.IsOnDown))
                    {
                        pair.Value.HasToExecute = keyboard.IsKeyDown(pair.Key.GameKey);

                        if (pair.Value.HasToExecute)
                        {
                            pair.Value.Execute();
                            if (!keysUp.Contains(pair.Value.GameKey))
                            {
                                keysUp.Add(pair.Value.GameKey);
                            }
                        }
                    }
                }
                 
                foreach (var pair in mapKeys.Where(p => !p.Key.IsOnDown))
                {
                    pair.Value.HasToExecute = keyboard.IsKeyUp(pair.Key.GameKey) && keysUp.Contains(pair.Value.GameKey);

                    if (pair.Value.HasToExecute)
                    {
                        pair.Value.Execute();
                        keysUp.Remove(pair.Value.GameKey);
                    }
                }
            }
        }

        public void GetKeyboardState()
        {

            if (Window.GetWindow().Focused)
            {
                var keyboard = Keyboard.GetState();

                if (keyboard.IsAnyKeyDown)
                    foreach (var pair in mapKeys)
                    {
                        //note that this will be improved
                        if(keyboard.IsKeyDown(pair.Key.GameKey));

                        if (pair.Value.HasToExecute)
                            pair.Value.Execute();
                    }
            }
        }
    }
}
