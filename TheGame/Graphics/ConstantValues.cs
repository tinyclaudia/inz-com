﻿namespace Graphics
{
    public static class ConstVal
    {
        public static int ViewScale = 4;
        
        public static int WidthResolution = 800;
        public static int HeightResolution = 600;

        public static int WidthView = WidthResolution / ViewScale;
        public static int HeightView = HeightResolution / ViewScale;

        public const string PathToComponents = @"..\..\..\Components\";
        public const string PathToShaders = @"..\..\..\Components\Shaders\";
        public const string PathToTextures = @"..\..\..\Components\Textures\";
        public const string DefaultVertexShaderFileName = @"..\..\..\Components\Shaders\vertexShader.vert";
        public const string DefaultFragmentShaderFileName = @"..\..\..\Components\Shaders\fragmentShader.frag";
    }
}
