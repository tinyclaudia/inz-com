﻿using OpenTK;
using System;
using OpenTK.Graphics.OpenGL;
using Graphics.Meshes;
using Graphics.Objects.Sprites;

namespace Graphics
{
    class ConstMesh : IMesh, IDisposable
    {
        protected int vertexArray;
        protected int verticeCount;
        protected Matrix4 model;
        private Vector2 animationOffset = new Vector2(0, 0);
        protected Shader shader;
        //Texture of mesh
        protected ISprite sprite;
        //switch from texcord to (0,1)x(0,1) coords
        public Matrix2 projectionTexture = Matrix2.Identity;

        private int[] vertexArrayBuffers = new int[2];

        protected virtual Shader CreateShader()
        {
            return new Shader();
        }

        public void TranslateLocation(float x, float y)
        {
            //add to cells in matrix
            model.M41 += x;
            model.M42 += y;
        }

        public int SpeedChangingSprite { get; set; }
        bool IMesh.IsAnimated
        {
            get { return false; }
            set {}
        }

        private void SendAttributesToGraphics(Vector4[] vertices, Vector2[] texCoords)
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexArrayBuffers[0]);
            GL.BufferData(BufferTarget.ArrayBuffer, verticeCount * Vector4.SizeInBytes, vertices, BufferUsageHint.StaticDraw);

            GL.EnableVertexAttribArray(0);//data in 0 atrib
            GL.VertexAttribPointer(0, 4, VertexAttribPointerType.Float, false, 0, 0);

            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexArrayBuffers[1]);
            GL.BufferData(BufferTarget.ArrayBuffer, verticeCount * Vector2.SizeInBytes, texCoords, BufferUsageHint.StaticDraw);

            GL.EnableVertexAttribArray(1);//data in 1 atrib
            GL.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, false, 0, 0);
        }

        private void CreateProjectionTexture()
        {
            //cells in matrix
            projectionTexture.M11 = 1.0f / sprite.Width;
            projectionTexture.M22 = 1.0f / sprite.Height;
        }

        public ConstMesh(Vector4[] vertices, Vector2[] texCoords, string textureFileName)
        {
            shader = CreateShader();
            sprite = new ConstSprite(textureFileName);
            CreateProjectionTexture();

            verticeCount = vertices.Length;

            model = Matrix4.Identity;

            vertexArray = GL.GenVertexArray();
            GL.BindVertexArray(vertexArray);

            GL.GenBuffers(2, vertexArrayBuffers);

            SendAttributesToGraphics(vertices, texCoords);

            GL.BindVertexArray(0);

            GL.UseProgram(shader.ProgramID);

            int projectionLoc = GL.GetUniformLocation(shader.ProgramID, "projection");
            GL.UniformMatrix4(projectionLoc, false, ref Camera.MainCamera.matrixProjection);

            int projectionTextureLoc = GL.GetUniformLocation(shader.ProgramID, "projectionTexture");
            GL.UniformMatrix2(projectionTextureLoc, false, ref projectionTexture);
        }

        public ConstMesh(Vector4[] vertices, Vector2[] texCoords, Sprite _sprite)
        {
            shader = CreateShader();
            sprite = _sprite;
            CreateProjectionTexture();

            verticeCount = vertices.Length;

            model = Matrix4.Identity;

            vertexArray = GL.GenVertexArray();
            GL.BindVertexArray(vertexArray);

            GL.GenBuffers(2, vertexArrayBuffers);

            SendAttributesToGraphics(vertices, texCoords);

            GL.BindVertexArray(0);

            GL.UseProgram(shader.ProgramID);

            int projectionLoc = GL.GetUniformLocation(shader.ProgramID, "projection");
            GL.UniformMatrix4(projectionLoc, false, ref Camera.MainCamera.matrixProjection);

            int projectionTextureLoc = GL.GetUniformLocation(shader.ProgramID, "projectionTexture");
            GL.UniformMatrix2(projectionTextureLoc, false, ref projectionTexture);

        }

        public void Render()
        {
            GL.UseProgram(shader.ProgramID);

            int modelLoc = GL.GetUniformLocation(shader.ProgramID, "model");
            GL.UniformMatrix4(modelLoc, false, ref model);

            Camera.MainCamera.Bind(shader.ProgramID);

            sprite.Bind(shader.ProgramID, 0);

            GL.BindVertexArray(vertexArray);
            GL.DrawArrays(PrimitiveType.Triangles, 0, verticeCount);
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {

                    GL.DeleteVertexArray(vertexArray);
                    //sprite.Dispose();
                    shader.Dispose();
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~Mesh() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }

        public void ChangeSprite(int i)
        {
        }

        public ISprite GetRenderedSprite()
        {
            return sprite;
        }

        public void NextSprite()
        {
        }

        #endregion
    }
}
