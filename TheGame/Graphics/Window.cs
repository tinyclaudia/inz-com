﻿using System;
using Graphics.Objects.Sprites;
using Graphics.Systems;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace Graphics
{
    public class Window : OpenTK.GameWindow
    {
        private static Window mainWindow = null;
        public static Window GetWindow()
        {
            mainWindow = mainWindow ?? new Window();

            return mainWindow;
        }

        public Window() : base(ConstVal.WidthResolution,ConstVal.HeightResolution)
        {

        }

        Mesh mesh;
        Mesh mesh2;
        Mesh mesh3;
        Mesh mesh4;
        Mesh mesh5;
        protected override void OnLoad(EventArgs e)
        {
            CursorVisible = true;
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
            GL.PatchParameter(PatchParameterInt.PatchVertices, 3);
            GL.Enable(EnableCap.Texture2D);

            Vector4[] vertices =
            {
                new Vector4( 0f, 100f, 0.0f, 1.0f),
                new Vector4( 100f, 0f, 0.0f, 1.0f),
                new Vector4( 0f, 0f, 0.0f, 1.0f),
                new Vector4( 100f, 0f, 0.0f, 1.0f),
                new Vector4( 0f, 100f, 0.0f, 1.0f),
                new Vector4( 100f, 100f, 0.0f, 1.0f),
            };

            Vector2[] texCoord =
            {
                new Vector2( 0.0f, 100f),
                new Vector2( 100f, 0.0f),
                new Vector2( 0.0f, 0.0f),
                new Vector2( 100f, 0f),
                new Vector2( 0f, 100f),
                new Vector2( 100f, 100f),

            };
            Sprite[] sprites = 
            {
                 new Sprite($@"{ConstVal.PathToTextures}RPG Sprites\spr_player_down_strip6.png", 6),
                 new Sprite($@"{ConstVal.PathToTextures}RPG Sprites\spr_player_up_strip6.png", 6) ,
                 new Sprite($@"{ConstVal.PathToTextures}RPG Sprites\spr_player_left_strip6.png", 6) ,
                 new Sprite($@"{ConstVal.PathToTextures}RPG Sprites\spr_player_right_strip6.png", 6) ,
            };

            Sprite[] sprites2 = 
            {
                new Sprite($@"{ConstVal.PathToTextures}dog2.png", 3) ,
            };

            mesh = Meshes.Tile.CreateTile(200, 200, 200, $"{ConstVal.PathToTextures}grass01.bmp");
            mesh2 = new Mesh(vertices, texCoord, $"{ConstVal.PathToTextures}texture02.jpg");
            mesh3 = Meshes.Tile.CreateTile(300, 1000, 1000, $"{ConstVal.PathToTextures}map.png");
            mesh4 = Meshes.Tile.CreateTile(14,21, 14, 21,sprites );

            mesh5 = Meshes.Tile.CreateTile(14,21, 52, 75,sprites2 );
            mesh5.TranslateLocation(25,30);
            mesh3.TranslateLocation(300, 300);

            Closed += OnClosed;
        }

        public void PlayerMoveEnd()
        {
            mesh4.IsAnimated = false;
        }

        private void OnClosed(object sender, EventArgs eventArgs)
        {
            Exit();
        }

        public override void Exit()
        {
            base.Exit();
        }
        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            InputHandler.keyboardHandler.Update();

            ChangingSpriteSystem.Update(e.Time);
        }

        private void SetTitleAndBackgroundColor(FrameEventArgs e)
        {
            Title = $" Game : (Vsync: {VSync}) FPS: {1f / e.Time:0}";

            Color4 backColor;
            backColor.A = 1.0f;
            backColor.R = 0.1f;
            backColor.G = 0.1f;
            backColor.B = 0.3f;
            GL.ClearColor(backColor);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            SetTitleAndBackgroundColor(e);

            RenderingSystem.Update();
            
            SwapBuffers();
        }
        //smrrrrooood
        public void PlayerMoveDown()
        {
            mesh4.IsAnimated = true;

            if(mesh4.IndexSprite != 0)
                mesh4.ChangeSprite(0);

            mesh4.TranslateLocation(0, speed);
            Camera.MainCamera.Translate(0, speed);
        }
        public void PlayerMoveUp()
        {
            mesh4.IsAnimated = true;

            if (mesh4.IndexSprite != 1)
                mesh4.ChangeSprite(1);
            mesh4.TranslateLocation(0, -speed);
            Camera.MainCamera.Translate(0, -speed);
        }
        public void PlayerMoveLeft()
        {
            mesh4.IsAnimated = true;

            isFirst = false;
            if (mesh4.IndexSprite != 2)
                mesh4.ChangeSprite(2);

            mesh4.TranslateLocation(-speed, 0);
            Camera.MainCamera.Translate(-speed, 0);
        }
        public void PlayerMoveRight()
        {
            mesh4.IsAnimated = true;

            isFirst = false;
            if (mesh4.IndexSprite != 3)
                mesh4.ChangeSprite(3);

            mesh4.TranslateLocation(speed, 0);
            Camera.MainCamera.Translate(speed, 0);
        }
        float speed = 5;
        bool isFirst = false;
        public void CameraZoomIn()
        {
            Camera.MainCamera.Zoom(0.01f);
        }
        public void CameraZoomOut()
        {
            Camera.MainCamera.Zoom(-0.01f);
        }

    }
}
