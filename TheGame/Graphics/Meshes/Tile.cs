﻿using Graphics.Objects.Sprites;
using OpenTK;

namespace Graphics.Meshes
{
    class Tile : Mesh
    {
        private Tile(Vector4[] vertices, Vector2[] texCoord, string textureFileName) :base(vertices,texCoord,textureFileName)
        {
        }
        private Tile(Vector4[] vertices, Vector2[] texCoord, Sprite sprite) : base(vertices, texCoord, sprite)
        {
        }
        private Tile(Vector4[] vertices, Vector2[] texCoord, ISprite [] sprite) : base(vertices, texCoord, sprite)
        {
        }

        public static Tile CreateTile(float size,float sizeTexX,float sizeTexY, string textureFileName)
        {
            Vector4[] vertices =
            {
                new Vector4( 0f, size, 0.0f, 1.0f),
                new Vector4( size, 0f, 0.0f, 1.0f),
                new Vector4( 0f, 0f, 0.0f, 1.0f),
                new Vector4( size, 0f, 0.0f, 1.0f),
                new Vector4( 0f, size, 0.0f, 1.0f),
                new Vector4( size, size, 0.0f, 1.0f),
            };

            Vector2[] texCoord =
            {
                new Vector2( 0.0f, sizeTexY),
                new Vector2( sizeTexX, 0.0f),
                new Vector2( 0.0f, 0.0f),
                new Vector2( sizeTexX, 0f),
                new Vector2( 0f, sizeTexY),
                new Vector2( sizeTexX, sizeTexY),

            };

            
            var tile = new Tile(vertices, texCoord, textureFileName);
           

            return tile;
        }

        public static Tile CreateTile(float sizeX,float sizeY, float sizeTexX, float sizeTexY, string textureFileName)
        {
            Vector4[] vertices =
            {
                new Vector4( 0f, sizeY, 0.0f, 1.0f),
                new Vector4( sizeX, 0f, 0.0f, 1.0f),
                new Vector4( 0f, 0f, 0.0f, 1.0f),
                new Vector4( sizeX, 0f, 0.0f, 1.0f),
                new Vector4( 0f, sizeY, 0.0f, 1.0f),
                new Vector4( sizeX, sizeY, 0.0f, 1.0f),
            };

            Vector2[] texCoord =
            {
                new Vector2( 0.0f, sizeTexY),
                new Vector2( sizeTexX, 0.0f),
                new Vector2( 0.0f, 0.0f),
                new Vector2( sizeTexX, 0f),
                new Vector2( 0f, sizeTexY),
                new Vector2( sizeTexX, sizeTexY),

            };


            var tile = new Tile(vertices, texCoord, textureFileName);


            return tile;
        }
        public static Tile CreateTile(float sizeX, float sizeY, float sizeTexX, float sizeTexY, Sprite sprite)
        {
            Vector4[] vertices =
            {
                new Vector4( 0f, sizeY, 0.0f, 1.0f),
                new Vector4( sizeX, 0f, 0.0f, 1.0f),
                new Vector4( 0f, 0f, 0.0f, 1.0f),
                new Vector4( sizeX, 0f, 0.0f, 1.0f),
                new Vector4( 0f, sizeY, 0.0f, 1.0f),
                new Vector4( sizeX, sizeY, 0.0f, 1.0f),
            };

            Vector2[] texCoord =
            {
                new Vector2( 0.0f, sizeTexY),
                new Vector2( sizeTexX, 0.0f),
                new Vector2( 0.0f, 0.0f),
                new Vector2( sizeTexX, 0f),
                new Vector2( 0f, sizeTexY),
                new Vector2( sizeTexX, sizeTexY),

            };


            var tile = new Tile(vertices, texCoord, sprite);


            return tile;
        }

        public static Tile CreateTile(float sizeX, float sizeY, float sizeTexX, float sizeTexY, ISprite [] sprite)
        {
            Vector4[] vertices =
            {
                new Vector4( 0f, sizeY, 0.0f, 1.0f),
                new Vector4( sizeX, 0f, 0.0f, 1.0f),
                new Vector4( 0f, 0f, 0.0f, 1.0f),
                new Vector4( sizeX, 0f, 0.0f, 1.0f),
                new Vector4( 0f, sizeY, 0.0f, 1.0f),
                new Vector4( sizeX, sizeY, 0.0f, 1.0f),
            };

            Vector2[] texCoord =
            {
                new Vector2( 0.0f, sizeTexY),
                new Vector2( sizeTexX, 0.0f),
                new Vector2( 0.0f, 0.0f),
                new Vector2( sizeTexX, 0f),
                new Vector2( 0f, sizeTexY),
                new Vector2( sizeTexX, sizeTexY),

            };


            var tile = new Tile(vertices, texCoord, sprite);


            return tile;
        }


    }
}
