﻿using System.Dynamic;
using Graphics.Objects.Sprites;

namespace Graphics.Meshes
{
    interface IMesh
    {
        void Render();

        void TranslateLocation(float x, float y);

        void ChangeSprite(int i);


        bool IsAnimated { get; set; }

        ISprite GetRenderedSprite();
    }
}
