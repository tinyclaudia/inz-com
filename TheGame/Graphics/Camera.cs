﻿using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Graphics
{
    class Camera
    {
        public static Camera MainCamera = new Camera(0,0);

        private float x, y, z;
        private Matrix4 matrixCamera;
        public Matrix4 matrixProjection;

        public void Bind(int programID)
        {
            int viewLoc = GL.GetUniformLocation(programID, "view");
            GL.UniformMatrix4(viewLoc, false, ref matrixCamera);

            int projectionLoc = GL.GetUniformLocation(programID, "projection");
            GL.UniformMatrix4(projectionLoc, false, ref matrixProjection);
        }

        public void Translate(float _x,float _y , float _z = 0)
        {
            x += _x;
            y += _y;
            z += _z;

            matrixCamera.M41 -= _x;
            matrixCamera.M42 -= _y;
        }
        float mainScale = 1;
        public void Zoom(float scale)
        {
            mainScale += scale;
            matrixProjection = Matrix4.CreateOrthographic(ConstVal.WidthView * mainScale, -ConstVal.HeightView * mainScale , -1.0f, 1.0f);
        }

        private void MakeMatrix()
        {
            Vector3 cameraPos = new Vector3(x, y, z);
            Vector3 cameraDirection = new Vector3(x, y, -1);

            Vector3 cameraUp = new Vector3(0, 1, 0);

            System.Console.WriteLine($"{cameraPos.ToString()}  : cameraPos");
            System.Console.WriteLine($"{cameraDirection.ToString()}  : cameraDirection");
            System.Console.WriteLine($"{cameraUp.ToString()}  : cameraUp");


            matrixCamera = Matrix4.LookAt(cameraPos, cameraDirection, cameraUp);
            matrixProjection = Matrix4.CreateOrthographic(ConstVal.WidthView * mainScale, -ConstVal.HeightView * mainScale, -1.0f, 1.0f);

            System.Console.WriteLine();
            System.Console.WriteLine($"{matrixCamera.ToString()}  : matrixCamera");
        }
       
        public Camera(float _x, float _y, float _z = 1)
        {
            x = _x;
            y = _y ;
            z = _z;

            MakeMatrix();
        }

    }
}
