﻿namespace Commons
{
    public class Skill
    {
        public Profficency Profficency { get; set; }
        public string Name;
        public string Description;

        /// <summary>
        /// Increases profficiency level.
        /// </summary>
        /// <Returns>false if already have reached maximum level. True otherwise.</Returns>
        public bool IncreaseProfficency()
        {
            switch (this.Profficency)
            {
                case Profficency.Expert:
                    return false;
                case Profficency.Beginner:
                    this.Profficency = Profficency.Advanced;
                    break;
                case Profficency.Advanced:
                    this.Profficency = Profficency.Expert;
                    break;
                default: break;
            }

            return true;
        }
    }
}
