﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons
{
    public abstract class Character
    {
        public string Name { get; }
        public Tier Tier { get; }
        public int Level { get; set; }

        public List<Skill> Skills;
        public List<EquipmentItem> equipment;

        public override string ToString()
        {
            return this.Name;
        }
    }
}
