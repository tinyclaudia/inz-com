﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons
{
    public class CommunicationPlayer : CommunicationGameObject
    {
        public bool IsMoving { get; set; }
        public bool StoppedMoving { get; set; }
        public int XAxis { get; set; }
        public int YAxis { get; set; }

        public CommunicationPlayer(int x, int y) : base(x, y) 
        {
            XAxis = 0;
            YAxis = 0;
            IsMoving = false;
            StoppedMoving = false;
        }
    }
}
