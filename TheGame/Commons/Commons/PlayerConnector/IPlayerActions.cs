﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Commons;

namespace Commons
{
    interface IPlayerConnector
    {
        void MoveBegin(Direction direction);

        void MoveEnd();

        void Atack();

        void PickItem();

        void DropItem(EquipmentItem item);

        void StartDialog();

        void GetCharacterInfo();

        void Suicide();

        void SendMessageToChat();
    }
}
