﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons
{
    /// <summary>
    /// Enum to determine Player's Tier
    /// </summary>
    public enum Tier { Knight, Wizard};

    /// <summary>
    /// Player's Skill level
    /// </summary>
    public enum Profficency { Beginner, Advanced, Expert };

    /// <summary>
    /// Actions that can be explicitly invoked by a Player 
    /// </summary>
    public enum PlayerAction { StartDialog };

    /// <summary>
    /// Types of objects that can be found on a GameMap instance
    /// </summary>
    public enum MapObjectType { Obstactle, Moveable };

    /// <summary>
    /// 4 possible directions
    /// </summary>
    public enum Direction { Left, Top, Right, Down };

    /// <summary>
    /// Type of quipment that can be carried by a Player
    /// </summary>
    public enum EquipmentType { armour };

    /// <summary>
    /// Types of messages Client (Player) can receive
    /// </summary>
    public enum GeneralMessageType { Update, Full, ClientRequest, SendId, SendMessage, SendInput };

    /// <summary>
    /// Message wraper for internal use. It makes keeping all types of messages in 
    /// a single collection feasible
    /// </summary>
    public enum MessageActionWrapperType { MessageFromServer, UserAction };

    /// <summary>
    /// Different types of update information that Player can receive from server
    /// </summary>
    public enum UpdateMessageType { ObjectMove, ObjectDestroy, PlayerInfo, TeamPlayerInfo, GameStatus };
}
