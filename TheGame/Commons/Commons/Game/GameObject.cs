﻿namespace Commons
{
    public class GameObject
    {
        // TODO: add graphical representation of the object 

        public string Name { get; }
        public MapObjectType Type { get; }
        public Position Position { get; }

        public GameObject(string _name, MapObjectType _type, Position _position)
        {
            this.Name = _name;
            this.Type = _type;
            this.Position = _position;
        }

        public override int GetHashCode()
        {
            return System.Tuple.Create(this.Name, this.Type).GetHashCode();
        }
    }
}
