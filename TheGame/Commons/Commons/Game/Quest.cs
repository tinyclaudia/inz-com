﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons
{
    public class Quest
    {
        private List<Task> tasks;

        public Quest()
        {
            tasks = new List<Task>();
        }

        public void AddTask(Task _task)
        {
            tasks.Add(_task);
        }
    }
}
