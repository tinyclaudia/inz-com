﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using logger;

namespace Commons
{
    public class GameBoard
    {
        public Field[,] fields;
        public int Size { get; }

        public GameBoard(int _size)
        {
            Size = _size;
        }

        public void UpdateGameBoard(UpdateServerInfo serverInfo)
        {
<<<<<<< HEAD
            //switch (serverInfo.type)
            //{
            //    case UpdateMessageType.ObjectMove:
            //        RemoveObject(serverInfo.OldPosition);
            //        AddObject(serverInfo.NewPosition, serverInfo.gameObject);
            //        break;
            //    case UpdateMessageType.ObjectDestroy:
            //        RemoveObject(serverInfo.OldPosition);
            //        break;
            //    default:
            //        Logger.LogError("Invalid message type");
            //        break;
            //}
=======
            switch(serverInfo.type)
            {
                case UpdateMessageType.ObjectMove:
                    RemoveObject(serverInfo.OldPosition);
                    AddObject(serverInfo.gameObject);
                    break;
                case UpdateMessageType.ObjectDestroy:
                    RemoveObject(serverInfo.OldPosition);
                    break;
                default:
                    Logger.LogError("Invalid message type");
                    break;
            }
>>>>>>> 353c2c848d12c7b94c8ed60627088b7e8ac196f0
        }

        public static GameBoard CreateGameBoard(FullServerInfo serverInfo)
        {
            var board = new GameBoard(serverInfo.Size);

            foreach (var item in serverInfo.Objects)
            {
<<<<<<< HEAD
                //board.AddObject(item.Item1, item.Item2);
=======
                board.AddObject(item);
>>>>>>> 353c2c848d12c7b94c8ed60627088b7e8ac196f0
            }

            return board;
        }

        public bool AddObject(GameObject mapObject)
        {
            if (!ValidatePosition(mapObject.Position))
                return false;

            fields[mapObject.Position.X, mapObject.Position.Y].Object = mapObject;
            return true;
        }

        public bool RemoveObject(Position position)
        {
            if (!ValidatePosition(position) || fields[position.X, position.Y].Object == null)
                return false;

            fields[position.X, position.Y].Object = null;
            return true;
        }

        private bool ValidatePosition(Position position)
        {
            return !(position.X < 0 || position.X > Size || position.Y < 0 || position.Y > Size);
        }
    }
}
