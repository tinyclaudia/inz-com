﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons
{
    public class CommunicationGameObject
    {
        static int maxId;

        public int Id { get; private set; }

        public int State { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

        public CommunicationGameObject(int x = 0, int y = 0)
        {
            State = 0;
            GenerateId();
            X = x;
            Y = y;
        }

        private void GenerateId()
        {
            maxId++;
            Id = maxId;
        }
    }

}
