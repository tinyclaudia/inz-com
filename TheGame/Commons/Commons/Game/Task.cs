﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons
{
    public class Task
    {
        public List<Task> predecessors { get; }
        public string Description { get; set; }
        public string Name { get; set; }
        public bool isDone { get; set; }


        public Task(string _Name = "", string _Description = "")
        {
            predecessors = new List<Task>();
            this.Name = _Name;
            this.Description = _Description;
        }

        public void AddPredecessor(Task task)
        {
            predecessors.Add(task);
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Description:");
            sb.AppendLine(this.Description + Environment.NewLine);

            sb.AppendLine("Requires:");
            predecessors?.ForEach(p => sb.AppendLine(p?.Name));

            return sb.ToString();
        }
    }
}
