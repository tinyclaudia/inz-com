﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons
{
    /// <summary>
    /// Pole musi posiadać dwie wartości:
    /// - Background czyli nasz drugi plan, coś co jest stale wyświetlane i wypełnia nam całe pole (trawa, rzeka, itp)
    /// - Object, opcjonalny obiekt, który wyświetla się na wierzchu (drzewo, przedmiot, itp)
    /// </summary>
    
    public class Field
    {
        // TODO: dodać background
        public GameObject Object;
    }
}
