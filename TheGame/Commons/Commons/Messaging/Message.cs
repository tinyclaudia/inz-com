﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Commons
{
    public class Message
    {
        [JsonProperty("MessageType")]
        public GeneralMessageType MessageType { get; set; }

        public Message(GeneralMessageType type)
        {
            this.MessageType = type;
        }

        // Serialize to Json
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        // Deserialize
        public static Message FromJson(string jsonData)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            };
            return JsonConvert.DeserializeObject<Message>(jsonData,settings);
        }

    }
}
