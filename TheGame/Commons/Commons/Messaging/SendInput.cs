﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Messaging
{
    public class SendInput : Message
    {
        public SendInput() : base(GeneralMessageType.SendInput)
        {
        }

        // Serialize to Json
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        // Deserialize
        public static SendInput FromJson(string jsonData)
        {
            return JsonConvert.DeserializeObject<SendInput>(jsonData);
        }
    }
}
