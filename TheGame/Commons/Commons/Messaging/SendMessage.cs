﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Messaging
{
    public class SendMessage : Message
    {
        [JsonProperty("Message")]
        public string Message { get; set; }

        public SendMessage(string message) : base(GeneralMessageType.SendMessage)
        {
            Message = message;
        }

        // Serialize to Json
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        // Deserialize
        public static SendMessage FromJson(string jsonData)
        {
            return JsonConvert.DeserializeObject<SendMessage>(jsonData);
        }
    }
}
