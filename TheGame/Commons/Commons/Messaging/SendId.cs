﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Messaging
{
    public class SendId : Message
    {
        public int Id { get; set; }
        public SendId(int id) : base(GeneralMessageType.SendId)
        {
            Id = id;
        }

        // Serialize to Json
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        // Deserialize
        public static SendId FromJson(string jsonData)
        {
            return JsonConvert.DeserializeObject<SendId>(jsonData);
        }
    }
}
