﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons
{
    public class FullServerInfo : Message
    {
        public List<GameObject> Objects { get; set; }
        //public List<Player> Players { get; set; }
        public Quest quest { get; set; }
        public int Size { get; set; }

        public FullServerInfo() : base(GeneralMessageType.Full)
        {
            Size = 0;
            Objects = new List<GameObject>();
            //Players = new List<Player>();
            quest = new Quest();
        }

        // Serialize to Json
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        // Deserialize
        public static Message FromJson(string jsonData)
        {
            return JsonConvert.DeserializeObject<Message>(jsonData);
        }
    }
}
