﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Messaging
{
    public class ClientRequest : Message
    {
        public int SenderId { get; set; }
        public string RequestType { get; set; }
        public ClientRequest (int senderId, string requestType) : base(GeneralMessageType.ClientRequest)
        {
            SenderId = senderId;
            RequestType = requestType;
        }

        // Serialize to Json
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        // Deserialize
        public static ClientRequest FromJson(string jsonData)
        {
            return JsonConvert.DeserializeObject<ClientRequest>(jsonData);
        }
    }
}
