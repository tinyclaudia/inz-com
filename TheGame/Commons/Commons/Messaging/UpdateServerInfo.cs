﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons
{
    public class UpdateServerInfo : Message
    {
        public List<CommunicationPlayer> Players { get; set; }

        public UpdateServerInfo(List<CommunicationPlayer> players) : base(GeneralMessageType.Update)
        {
            Players = players;
        }

        // Serialize to Json
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        // Deserialize
        public static UpdateServerInfo FromJson(string jsonData)
        {
            return JsonConvert.DeserializeObject<UpdateServerInfo>(jsonData);
        }
    }
}
