﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using CodeExMachina;

namespace Commons
{
    public static class CommunicationClientQueue
    {
        private const int MAX_QUEUE_SIZE = 10;

        private static Game game;
        private static PlayerInfo playerInfo;
        private static List<MessageActionWrapper> queue;

        private static object queueLock = new object();
        private static ConditionVariable notFullCondition = new ConditionVariable();
        private static ConditionVariable notEmptyCondition = new ConditionVariable();

        public static void Start()
        {
            var MessageThread = new Thread(new ThreadStart(ReceiveMessages));
            var ActionThread = new Thread(new ThreadStart(WaitForUserAction));

            while (game.IsActive)
            {
                var item = Take();
                switch (item.Type)
                {
                    case MessageActionWrapperType.UserAction:

                        break;
                    case MessageActionWrapperType.MessageFromServer:

                        break;
                }

            }
        }

        public static MessageActionWrapper Take()
        {

            lock (queueLock)
            {
                while (queue.Count == 0)
                {
                    // wait for queue to be not empty
                    notEmptyCondition.Wait(queueLock);
                }

                MessageActionWrapper item = queue.ElementAt(0);
                queue.RemoveAt(0);

                if (queue.Count < MAX_QUEUE_SIZE)
                {

                    // notify producer queue not full anymore
                    notFullCondition.Pulse();
                }

                return item;
            }
        }

        public static void AddToQueue(MessageActionWrapper item)
        {
            lock (queueLock)
            {
                while (queue.Count >= MAX_QUEUE_SIZE)
                {
                    // wait for queue to be not full
                    notFullCondition.Wait(queueLock);
                }

                queue.Add(item);

                // notify consumer queue not empty anymore
                notEmptyCondition.Pulse();
            }
        }

        private static void ReceiveMessages()
        {
            // TODO

            //AddToQueue(new MessageActionWrapper(new UpdateServerInfo()));
        }

        private static void WaitForUserAction()
        {
            // TODO

            AddToQueue(new MessageActionWrapper(new UserAction()));
        }

    }

    public class PlayerInfo
    {

    }

    public class MessageActionWrapper
    {
        public MessageActionWrapperType Type;

        private UpdateServerInfo serverInfo;
        private UserAction userAction;

        public MessageActionWrapper(UserAction _userAction)
        {
            userAction = _userAction;
            Type = MessageActionWrapperType.UserAction;
        }

        public MessageActionWrapper(UpdateServerInfo _serverInfo)
        {
            serverInfo = _serverInfo;
            Type = MessageActionWrapperType.MessageFromServer;
        }

        public object GetContent()
        {
            switch(this.Type)
            {
                case MessageActionWrapperType.MessageFromServer:
                    return serverInfo;
                case MessageActionWrapperType.UserAction:
                    return userAction;
                default:
                    return null;
            }
        }
    }

}
