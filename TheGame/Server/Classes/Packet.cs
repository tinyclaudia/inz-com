﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame
{
    
        public class Packet
        {
            [JsonProperty("command")]
            public string Command { get; set; }

            [JsonProperty("senderID")]
            public string SenderId { get; set; }

            [JsonProperty("playerID")]
            public string PlayerId { get; set; }

            [JsonProperty("playerX")]
            public string PlayerX { get; set; }

            [JsonProperty("playerY")]
            public string PlayerY { get; set; }

            [JsonProperty("playerState")]
            public string PlayerState { get; set; }

            // Makes a packet
            public Packet(string command = "", string senderId = "-1", string playerId = "", string playerX = "0", string playerY = "0", string playerState = "")
            {
                Command = command;
                SenderId = senderId;
                PlayerId = playerId;
                PlayerX = playerX;
                PlayerY = playerY;
                PlayerState = playerState;
            }

            public override string ToString()
            {
                return string.Format(
                    "[Packet:\n" +
                    "  Command=`{0}`\n" +
                    "  PlayerId=`{1}`]",
                    Command, PlayerId);
            }

            // Serialize to Json
            public string ToJson()
            {
                return JsonConvert.SerializeObject(this);
            }

            // Deserialize
            public static Packet FromJson(string jsonData)
            {
                return JsonConvert.DeserializeObject<Packet>(jsonData);
            }
        }
    

}
