﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;
using Commons.Messaging;

namespace TheGame
{
    public class Game : IGame
    {
        // Objects for the game
        private Server _server;
        private List<TcpClient> _tcpPlayer = new List<TcpClient>();
        private Random _rng;
        private int _lastPlayer = 0;
        private bool _needToDisconnectClient = false;

        private Thread updateGameState;
        private List<CommunicationPlayer> _player = new List<CommunicationPlayer>();

        // Name of the game
        public string Name
        {
            get { return "Hate it"; }
        }

        // Just needs only one player
        public int RequiredPlayers
        {
            get { return 2; }
        }

        // Constructor
        public Game(Server server)
        {
            updateGameState = new Thread(updateGame);
            _server = server;
            _rng = new Random();
        }

        // Adds only a single player to the game
        public bool AddPlayer(TcpClient client)
        {
            // Make sure only one player was added
            if (_tcpPlayer != null)
            {
                _tcpPlayer.Add(client);
                _player.Add(new CommunicationPlayer(0, 0));
                _lastPlayer++;
                return true;
            }

            return false;
        }

        // If the client who disconnected is ours, we need to quit our game
        public void DisconnectClient(TcpClient client)
        {
            _needToDisconnectClient = (_tcpPlayer.Contains(client));
        }

        //update game state asynchronically
        private void updateGame()
        {
            while(true)
            {
                List<CommunicationPlayer> updatePlayers = new List<CommunicationPlayer>();
                for (int j = 0; j < _lastPlayer; j++)
                {
                    if (_player[j].IsMoving)
                    {
                        _player[j].X += _player[j].XAxis;
                        _player[j].Y += _player[j].YAxis;
                        updatePlayers.Add(_player[j]);
                    }
                    else if(_player[j].StoppedMoving)
                    {
                        _player[j].StoppedMoving = false;
                        _player[j].X += _player[j].XAxis;
                        _player[j].Y += _player[j].YAxis;
                        updatePlayers.Add(_player[j]);
                    }
                }

                UpdateServerInfo updateGameState = new UpdateServerInfo(updatePlayers);

                for (int i = 0; i < _lastPlayer; i++)
                {
                    _server.SendPacket(_tcpPlayer[i], updateGameState).GetAwaiter().GetResult();
                }
                
                Thread.Sleep(1000);
            }
        }


        // Main loop of the Game
        // Packets are sent sent synchronously though
        public void Run()
        {
            // Make sure we have a player
            bool running = (_tcpPlayer != null);
            if (running)
            {
                for (int i = 0; i < _lastPlayer; i++)
                {
                    Commons.Messaging.SendId sendId = new Commons.Messaging.SendId(_player[i].Id);
                    Console.WriteLine("SendId sent");
                    _server.SendPacket(_tcpPlayer[i], sendId).GetAwaiter().GetResult();
                }
                // Send a instruction packet
                SendMessage introPacket = new SendMessage(
                    "Welcome player, where would you like to move?\n" +
                    "Right, left, up, down.\n");
                for (int i = 0; i < _lastPlayer; i++)
                {
                    Console.WriteLine("intro sent");
                    _server.SendPacket(_tcpPlayer[i], introPacket).GetAwaiter().GetResult();
                }
                updateGameState.Start();
            }
            else
                return;

            // Some bools for game state
            bool clientConncted = true;
            bool clientDisconnectedGracefully = false;

            // Main game loop
            while (running)
            {
                // Poll for input
                SendInput inputPacket = new SendInput();
                for (int i = 0; i < _lastPlayer; i++)
                {
                    _server.SendPacket(_tcpPlayer[i], inputPacket).GetAwaiter().GetResult();
                }

                // Read their answer
                Commons.Message answerPacket = null;
                while (answerPacket == null)
                {
                    for (int i = 0; i < _lastPlayer; i++)
                    {
                        answerPacket = _server.ReceivePacket(_tcpPlayer[i]).GetAwaiter().GetResult();
                    }
                    Thread.Sleep(10);
                }

                // Check for graceful disconnect
                //if (answerPacket.Command == "bye")
                //{
                //    _server.HandleDisconnectedClient(_tcpPlayer[_lastPlayer-1]);
                //    clientDisconnectedGracefully = true;
                //}

                // Check input
                if (answerPacket.MessageType == Commons.GeneralMessageType.ClientRequest)
                {
                    //Message responsePacket = new Packet();

                    ClientRequest answer = answerPacket as ClientRequest;
                    string playerCommand = answer.RequestType;

                    int _playerId = 0;
                    for(int i=0; i<_lastPlayer; i++)
                    {
                        if(answer.SenderId == _player[i].Id)
                        {
                            _playerId = i;
                        }
                    }
                   
                    if (playerCommand.Equals("startMoveUp"))
                    {
                        _player[_playerId].IsMoving = true;
                        _player[_playerId].YAxis = 1;
                        _player[_playerId].State = 1;
                       // responsePacket.Command = "ok";
                    }

                    if (playerCommand.Equals("startMoveDown"))
                    {
                        _player[_playerId].IsMoving = true;
                        _player[_playerId].YAxis = -1;
                        _player[_playerId].State = 2;
                       // responsePacket.Command = "ok";
                    }

                    if (playerCommand.Equals("startMoveLeft"))
                    {
                        _player[_playerId].IsMoving = true;
                        _player[_playerId].XAxis = -1;
                        _player[_playerId].State = 3;
                       // responsePacket.Command = "ok";
                    }

                    if (playerCommand.Equals("startMoveRight"))
                    {
                        _player[_playerId].IsMoving = true;
                        _player[_playerId].XAxis = 1;
                        _player[_playerId].State = 4;
                       // responsePacket.Command = "ok";
                    }

                    if(playerCommand.Equals("endMoveUp") || playerCommand.Equals("endMoveDown"))
                    {
                        _player[_playerId].IsMoving = false;
                        _player[_playerId].XAxis = 0;
                        _player[_playerId].State = 0;
                        _player[_playerId].StoppedMoving = true;
                      //  responsePacket.Command = "ok";
                    }

                    if(playerCommand.Equals("endMoveLeft") || playerCommand.Equals("endMoveRight"))
                    {
                        _player[_playerId].IsMoving = false;
                        _player[_playerId].StoppedMoving = true;
                        _player[_playerId].YAxis = 0;
                        _player[_playerId].State = 0;
                       // responsePacket.Command = "ok";
                    }
                    
                    //else 
                       // responsePacket.Message = "Player moved the other direction\n";
                   
                 

                    // Send the message
                    //_server.SendPacket(_tcpPlayer[_playerId], responsePacket).GetAwaiter().GetResult();
                }



                // Take a small nap
                Thread.Sleep(10);


                // Check for disconnect, may have happend gracefully before
                if (!_needToDisconnectClient && !clientDisconnectedGracefully)
                    clientConncted &= !Server.IsDisconnected(_tcpPlayer[_lastPlayer-1]);
                else
                    clientConncted = false;

                running &= clientConncted;
            }

            // Thank the player and disconnect them
            if (clientConncted)
                _server.DisconnectClient(_tcpPlayer[_lastPlayer-1], "Thanks for playing \"Hate it\"!");
            else
                Console.WriteLine("Client disconnected from game.");

            Console.WriteLine("Ending a \"{0}\" game.", Name);
        }
    }
}
